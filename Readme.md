
# MySQL + R(Studio)

1. [ODBC](#odbc-conector) 
   1. [Windows](#windows)
   2. [Linux](#linux) 
3. [Parámetros de Conexión](#parámetros-de-conexión)
4. [Entidad-Relación](#esquema-relacional)  
5. [Repositorio (Detalle)](#repositorio)
 
## Inicial 

Este repositorio contiene los materiales con los que se desarrollará el así denominado Minicurso: MySQL + RStudio. El objetivo del mismo es demostrar como se puede acceder a fuentes de datos externas, puntualmente Bases de Datos Relacionales, en el entorno de RStudio.  A su vez, se presentarán brevemente los fundamentos en los que se soporta dicha estrategia de trabajo.  

La jornada propondrá una breve contextualización, presentación de herramientas y concluirá con una demostración.  

A continuación se brindan instrucciones orientativas para aquellos que deseen ir replicando el trabajo que se presentará en el apartado de demostración.  Posteriormente se brindan los parámetros de conección, se brinda el esquema relacional de la BD con la que se trabajará y una breve descripción de lo que puede encontrar en el repositorio.

### ODBC Conector 

#### Windows  

Previamente verifique que posee Visual Studio 2019, puede descargar y realizar la instalación típica (aprete *next*) desde la página: https://visualstudio.microsoft.com/es/downloads/
1. Dirijase a: https://downloads.mysql.com/archives/c-odbc/
2. Seleccione la versión 8.0.22.  
3. Seleccione SO Windows.
4. Especifique la versión del SO.  
5. Descargue el paquete MSI Installer (2 a 6 minutos).
6. Corra el instalador (seleccione instalación típica).  

#### Linux

1. Descarge de https://downloads.mysql.com/archives/c-odbc/
o bien 
> wget  https://dev.mysql.com/downloads/file/?id=501118

2. Extraer archivo   
>  <span style="color: darkblue"> gunzip mysql-connector-odbc-8.0.24-i686-pc-linux.tar.gz </span>   
> <span style="color: blue">tar xvf mysql-connector-odbc-8.0.24-i686-pc-linux.tar

3. Copiar los subdirectorios directorios  <span style= "color: blue"> bin </span> y <span style= "color: blue"> lib </span> en los directorios del sistema.

> cp /bin/* /usr/local/bin  
> cp /lib/* /usr/local/lib  

4. Registre los drivers  

> myodbc-installer -a -d -n "MySQL ODBC 8.0 Driver" -t "Driver=/usr/local/lib/libmyodbc8w.so"  


> myodbc-installer -a -d -n "MySQL ODBC 8.0" -t "Driver=/usr/local/lib/libmyodbc8a.so"  

5. Verifique  

>  myodbc-installer -d -l

<img src= "img/Drivers1.png" width = "500">


### Parámetros de conexión 

#### <span style="color: blue"> Base alojada en Clever Cloud </span>

* driver: "MySQL"  o equivalente  
* base de datos = <span style="color: green"> byo7idkx5pni1l1uiket </span>
* usuario: <span style="color: green"> usdqgqyhajy8ahh1 </span>
* password: <span style="color: green"> 0qWVcphp1FgqSuBTUyS3  </span>
* servidor: byo7idkx5pni1l1uiket-mysql.services.clever-cloud.com
* puerto: 3306  

### Esquema Relacional   

<img src= "img/ER_BDRET.png">

### Repositorio  

En el repositorio se encuentran
* Archivos que generan la presentación
   - presentacion.Rmd
   - presentacion.html
   - /img  

* Archivos que generan la base de datos para aquellos que quieran replicarlos en su servidor local 
   * /sql
   * /tablas 


* **Guia.Rmd** tiene las metas a cumplir durante la parte aplicada. 
  
> <div class="alert alert-block alert-danger"><b> Aclaración: </b>  Guia.Rmd será empleado por el expositor y aquellos que quieran ir replicando sincronicamente. El resto podrá replicarlo asincronicamente en base al material audiovisual resultante del mini-curso.</div>